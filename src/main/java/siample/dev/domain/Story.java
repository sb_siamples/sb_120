package siample.dev.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Story {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String title;
	private String content;
	private String author;
	private Date posted;
	
	@ManyToOne
	private Blogger blogger;

	public Story() {
		
	}

	public Story(Long id, String title, String content, String author, Date posted, Blogger blogger) {
		this.id = id;
		this.title = title;
		this.content = content;
		this.author = author;
		this.posted = posted;
		this.blogger = blogger;
	}

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getContent() {
		return content;
	}


	public void setContent(String content) {
		this.content = content;
	}


	public String getAuthor() {
		return author;
	}


	public void setAuthor(String author) {
		this.author = author;
	}


	public Date getPosted() {
		return posted;
	}


	public void setPosted(Date posted) {
		this.posted = posted;
	}


	public Blogger getBlogger() {
		return blogger;
	}


	public void setBlogger(Blogger blogger) {
		this.blogger = blogger;
	}
	
	
	
}
