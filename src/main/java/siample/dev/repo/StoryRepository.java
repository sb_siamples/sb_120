package siample.dev.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import siample.dev.domain.Story;

public interface StoryRepository extends CrudRepository<Story, Long> {
	
	List<Story> findAll(); // felulirja Iterable-t
	// JDBC háttérben futtatja a SELECT * FROM story; sql-t.
	
	
}
